using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysFaceCamera : MonoBehaviour
{
    private Transform m_camera;

    void Awake()
    {
        m_camera = Camera.main.transform;
    }

    private void Start()
    {
        LookAtCamera();
    }

    private void LookAtCamera()
    {
        transform.LookAt(m_camera);
    }
}
