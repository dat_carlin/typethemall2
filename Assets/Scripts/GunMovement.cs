using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class GunMovement : MonoBehaviour
{

    public static GunMovement Instance;

    [BoxGroup("References"), SerializeField] private Transform m_lookTarget;
    [BoxGroup("Settings"), SerializeField] private Vector3 m_lookTargetDefaultPosition;

    private Touch m_touch;
    private float m_speedModifire = 0.003f;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        SetTargetDefaultPosition();
    }

    private void Update()
    {
        LookAtTarget();
        FollowForFinger();
    }

    private void LookAtTarget()
    {
        transform.LookAt(m_lookTarget);
    }

    public void SetTargetDefaultPosition()
    {
        m_lookTarget.localPosition = m_lookTargetDefaultPosition;
    }

    private void FollowForFinger()
    {
        if (Input.touchCount > 0)
        {
            m_touch = Input.GetTouch(0);

            if (m_touch.phase == TouchPhase.Moved)
            {
                m_lookTarget.transform.position = new Vector3(
                    m_lookTarget.position.x + m_touch.deltaPosition.x * m_speedModifire,
                    m_lookTarget.position.y + m_touch.deltaPosition.y * m_speedModifire,
                    m_lookTarget.transform.position.z);
            }
        }


    }
}
