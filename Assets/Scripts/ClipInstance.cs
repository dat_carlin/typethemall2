using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;

public class ClipInstance : MonoBehaviour
{
    public static ClipInstance Instance;

    [BoxGroup("References"), SerializeField] private GameObject m_bulletPrefab;

    private TouchScreenKeyboard touchScreenKeyboard;
    private GunInstance m_gunInstance;

    public bool CanOpenKeyBoard()
    {
        bool canOpen = false;

        if (touchScreenKeyboard != null)
        {
            if (m_clipText.text.Length <= 7)
            {
                canOpen = true;
            }
        }
        return canOpen;
    }
    [HideInInspector]
    public int m_chargedLettersCount()
    {
        int letters;
        letters = m_clipText.text.Length;
        return letters;
    }

    [BoxGroup("References"), SerializeField] private TextMeshPro m_clipText;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        m_gunInstance = GunInstance.Instance;
        m_clipText.text = "";
    }

    void Update()
    {
        if (touchScreenKeyboard == null)
            return;
        if (m_clipText.text.Length <= 7)
        {
            m_clipText.text = touchScreenKeyboard.text;
            //m_chargedLettersCount = m_clipText.text.Length;
            //print(m_chargedLettersCount);
        }
        else
        {
            m_gunInstance.ZoomOutGun();
            touchScreenKeyboard.active = false;
        }
    }

    public void OpenKeyboard()
    {
        touchScreenKeyboard = TouchScreenKeyboard.Open(m_clipText.text, TouchScreenKeyboardType.Default, false, false, true);
        TouchScreenKeyboard.hideInput = true;

        if (!CanOpenKeyBoard())
        {
            touchScreenKeyboard.active = false;
        }
    }

    public void ShootLetter()
    {
        if (m_clipText.text.Length > 0)
        {
            string oldString = m_clipText.text;
            string newString;
            char uselessLetter = oldString[0];
            newString = oldString.Remove(0, 1);
            m_clipText.text = newString;
            touchScreenKeyboard.text = m_clipText.text;
            m_gunInstance.PlayShotAnimation();

            GameObject bulletObject = Instantiate(m_bulletPrefab, m_gunInstance.m_startShootPosition.transform.position, transform.rotation);
            BulletInstance bulletInstance = bulletObject.GetComponent<BulletInstance>();
            bulletInstance.SetupAndShootBullet(uselessLetter);

        }
        else
        {
            m_gunInstance.PlayIdleAnimation();
        }
    }
}
