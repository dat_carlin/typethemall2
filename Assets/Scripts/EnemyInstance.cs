using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;

public enum EnemyType
{
    Character,
    Static
}

public class EnemyInstance : MonoBehaviour
{
    [BoxGroup("References"), SerializeField] private Transform m_lettersHolder;
    [BoxGroup("References"), SerializeField] private GameObject m_deathParticle;

    [BoxGroup("Settings"), SerializeField] private EnemyType m_enemyType;

    private List<TextMeshPro> m_defaultLettersList = new List<TextMeshPro>();
    private List<TextMeshPro> m_usingLettersList = new List<TextMeshPro>();

    private SplineController m_splineController;

    private int m_currentLife;

    private void Start()
    {
        m_splineController = SplineController.Instance;
        SetupLettersList();
    }

    private void SetupLettersList()
    {
        for (int i = 0; i < m_lettersHolder.childCount; i++)
        {
            m_defaultLettersList.Add(m_lettersHolder.GetChild(i).GetComponent<TextMeshPro>());
        }

        m_usingLettersList.AddRange(m_defaultLettersList);

        m_currentLife = m_defaultLettersList.Count;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<BulletInstance>())
        {
            BulletInstance bullet = collision.gameObject.GetComponent<BulletInstance>();
            bullet.PlayBulletSpark();
            CheckBulletLetter(bullet.m_letterChar);
            bullet.gameObject.SetActive(false);
        }
    }

    private void CheckBulletLetter(char letter)
    {
        if (m_usingLettersList[0].text == letter.ToString())
        {
            m_usingLettersList[0].gameObject.GetComponent<Animator>().Play("LetterStages");
            m_usingLettersList.RemoveAt(0);
            m_currentLife--;
            CheckLifes();
        }
    }


    private void CheckLifes()
    {
        if (m_currentLife <= 0)
        {
            Instantiate(m_deathParticle, transform.position, Quaternion.Euler(new Vector3(-90, 0, 0)));
            MakeActionAfterDeath();
            Invoke(nameof(FixateDeath), 0.75f);
        }
    }

    private void FixateDeath()
    {
        m_splineController.SetNewSpline();
    }

    private void MakeActionAfterDeath()
    {
        switch (m_enemyType)
        {
            case EnemyType.Character:
                {
                    gameObject.GetComponent<RagdollController>().EnableRagdoll();
                    break;
                }
            case EnemyType.Static:
                {
                    gameObject.SetActive(false);
                    break;
                }
        }
    }
}
