using System.Collections;
using System.Collections.Generic;
using Dreamteck.Splines;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;

public class SplineController : MonoBehaviour
{
    public static SplineController Instance;

    [BoxGroup("References"), SerializeField] private SplineFollower m_splineFollower;
    [BoxGroup("References"), SerializeField] private List<SplineComputer> m_splineComputers = new List<SplineComputer>();
    [BoxGroup("References"), SerializeField] private SplineComputer m_finishSpline;

    private bool m_canEndLevel;

    private void Awake()
    {
        Instance = this;
        m_splineFollower.onEndReached += FixateArriving;
    }

    private void Start()
    {
        SetNewSpline();
    }


    private void FixateArriving(double some)
    {
        CheckAvailableEndLevel();
        //SetNewSpline();
    }

    public void SetNewSpline()
    {

        if (m_splineComputers.Count > 0)
        {
            m_splineFollower.spline = m_splineComputers[0];
            m_splineFollower.SetDistance(0f);
            m_splineComputers.RemoveAt(0);
        }
        else
        {
            m_splineFollower.spline = m_finishSpline;
            m_splineFollower.SetDistance(0f);
            m_canEndLevel = true;
        }

    }

    private void CheckAvailableEndLevel()
    {
        if (m_canEndLevel)
        {
            LevelController.Instance.FixateWinLevel();
        }
    }
}
