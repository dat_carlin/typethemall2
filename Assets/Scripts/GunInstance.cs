using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public enum GunState
{
    Typing,
    NoTyping
}

[RequireComponent(typeof(Animator))]
public class GunInstance : MonoBehaviour
{
    public static GunInstance Instance;

    [BoxGroup("References")] public Transform m_startShootPosition;

    private Animator m_animator;
    private ClipInstance m_clipInstance;
    private GunMovement m_gunMovement;


    [HideInInspector] public GunState m_gunState;

    private bool m_canShootFromLaser;
    private bool m_canShootLetter;
    private bool isShooting;

    private float m_bulletsDelay = 0.35f;

    private void Awake()
    {
        Instance = this;
        m_animator = GetComponent<Animator>();

    }

    private void Start()
    {
        m_clipInstance = ClipInstance.Instance;
        m_gunMovement = GunMovement.Instance;
    }

    private void OnMouseDown()
    {
        SwitchGun();
    }

    private void SwitchGun()
    {
        switch (m_gunState)
        {
            case GunState.NoTyping:
                {
                    ZoomOutGun();
                    break;
                }
            case GunState.Typing:
                {
                    ZoomInGun();
                    break;
                }
        }
    }

    private void Update()
    {
        switch (m_gunState)
        {
            case GunState.NoTyping:
                {
                    m_animator.SetFloat("GunState", 1f, 1f, Time.smoothDeltaTime * 10f);
                    break;
                }
            case GunState.Typing:
                {
                    m_animator.SetFloat("GunState", 0f, 1f, Time.smoothDeltaTime * 10f);
                    break;
                }
        }

    }

    private void FixedUpdate()
    {
        ShootLaser();
    }


    #region Zooming
    public void ZoomOutGun()
    {
        m_gunState = GunState.Typing;
        if (m_clipInstance.m_chargedLettersCount() > 0)
        {
            ActivateLaser();
            StartCoroutine(EnableShooting(0.5f));
        }
        //m_animator.SetFloat("GunState", 1f);
    }

    public void ZoomInGun()
    {
        m_gunState = GunState.NoTyping;
        m_gunMovement.SetTargetDefaultPosition();
        m_clipInstance.OpenKeyboard();
        //m_animator.SetFloat("GunState", 0f);
    }
    #endregion


    #region Shooting
    public void PlayShotAnimation()
    {
        m_animator.Play("Gun_Shot");
    }

    public void PlayIdleAnimation()
    {
        m_animator.enabled = false;
        m_animator.enabled = true;
        //m_gunMovement.SetTargetDefaultPosition();
        m_animator.Play("IdleBlendTree");

    }

    private void ActivateLaser()
    {
        m_canShootFromLaser = true;
    }

    private void DisableLaser()
    {
        m_canShootFromLaser = false;
    }

    private void ShootLaser()
    {
        if (m_canShootFromLaser)
        {
            ShootRayForward();
        }
    }


    private void ShootRayForward()
    {
        var fwd = m_startShootPosition.transform.position - m_startShootPosition.transform.TransformDirection(transform.forward * 25f);
        Ray ray = new Ray(m_startShootPosition.transform.position, m_startShootPosition.transform.position - fwd);
        Debug.DrawRay(m_startShootPosition.transform.position, m_startShootPosition.transform.position - fwd, Color.red, 2f);
        if (Physics.Raycast(ray, out RaycastHit hit, 200f))
        {
            if (hit.transform.gameObject.GetComponent<EnemyInstance>())
            {
                CheckShotAbility();
            }
        }
    }


    public IEnumerator EnableShooting(float delay)
    {
        yield return new WaitForSeconds(delay);
        m_canShootLetter = true;
        yield return new WaitForFixedUpdate();
        m_canShootLetter = false;
        yield return new WaitForSeconds(m_bulletsDelay);
        StartCoroutine(EnableShooting(0f));
    }

    public void DisableShooting()
    {
        m_canShootLetter = false;
    }

    private void ShootLetter()
    {

        m_clipInstance.ShootLetter();
    }



    private void CheckShotAbility()
    {
        if (isShooting) return;
        isShooting = true;
        ShootLetter();
        Invoke(nameof(ResetShoot), m_bulletsDelay);
    }


    void ResetShoot()
    {
        isShooting = false;
    }

    #endregion
}
