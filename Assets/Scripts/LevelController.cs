using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    public static LevelController Instance;

    [Header("Progress Bar Things")]
    [SerializeField] private Slider m_progressBar;
    [SerializeField] private Transform m_playerObject;
    [SerializeField] private Transform m_finish;
    [SerializeField] private Text m_currentLevelText;
    [SerializeField] private Text m_nextLevelText;

    [Header("Finish Things")]
    [SerializeField] private Animator m_winAnimator;
    [SerializeField] private Button m_nextLevelButton;

    [Header("Level Settings")]
    [SerializeField] private GameObject[] m_uselessUI;


    private int m_currentLevel;
    private float m_progressBarMaxValue;

    private void Awake()
    {
        Time.timeScale = 1f;
        AnalyticsManager.Initialize();
        Instance = this;
        m_nextLevelButton.onClick.AddListener(StartNextLevel);
    }

    private void Start()
    {
        SetupScrollBar();
        PrepareCurrentLevel();
        FixateStartLevelAnalytics();
    }

    public void FixateWinLevel()
    {
        SaveNewLevel();
        Time.timeScale = 0f;
        HideUselessUI();
        ShowWinPanel();
        FixateWinAnalysics();
        //StartNextLevel();
    }

    private void HideUselessUI()
    {
        for (int i = 0; i < m_uselessUI.Length; i++)
        {
            m_uselessUI[i].SetActive(false);
        }
    }

    private void ShowWinPanel()
    {
        m_winAnimator.Play("ShowWinPanel");
    }

    public void StartNextLevel()
    {
        if (SceneManager.GetActiveScene().buildIndex <= SceneManager.sceneCount)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }

    private void Update()
    {
        CalculateProgressBarValue();
    }


    private void SetupScrollBar()
    {
        m_progressBar.maxValue = Vector3.Distance(m_playerObject.position, m_finish.transform.position);
        m_progressBarMaxValue = m_progressBar.maxValue;
    }

    private void CalculateProgressBarValue()
    {
        m_progressBar.value = m_progressBarMaxValue - Vector3.Distance(m_playerObject.transform.position, m_finish.transform.position);
    }

    private void PrepareCurrentLevel()
    {
        if (PlayerPrefs.HasKey("CurrentLevelNum"))
        {
            SetLevelsText(PlayerPrefs.GetInt("CurrentLevelNum"));
            m_currentLevel = PlayerPrefs.GetInt("CurrentLevelNum");
        }
        else
        {
            SetLevelsText(1);
            m_currentLevel = 1;
            PlayerPrefs.SetInt("CurrentLevelNum", 1);
        }
    }

    private void SetLevelsText(int currentLevel)
    {
        m_currentLevelText.text = currentLevel.ToString();
        m_nextLevelText.text = (currentLevel + 1).ToString();
    }

    private void SaveNewLevel()
    {
        PlayerPrefs.SetInt("CurrentLevelNum", PlayerPrefs.GetInt("CurrentLevelNum") + 1);
    }

    private void FixateWinAnalysics()
    {
        AnalyticsManager.FireRoundCompleteEvent(m_currentLevel, 0, 0);
        AnalyticsManager.FireRoundEndEvent(m_currentLevel, 0, 0);
    }

    private void FixateLoseAnalytics()
    {
        AnalyticsManager.FireRoundFailEvent(m_currentLevel, 0, 0, 0);
        AnalyticsManager.FireRoundEndEvent(m_currentLevel, 0, 0);
    }

    private void FixateStartLevelAnalytics()
    {
        AnalyticsManager.FireRoundStartEvent(m_currentLevel, 0);
    }
}
