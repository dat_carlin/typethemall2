using Facebook.Unity;
using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * - You don't need to add any object on your game scene to have this class to work properly.
 * - Just call Initialize() method of this static class from Awake method of your game controller class (or from another place on application start)
 * - This analytics manager can handle some problems with internet connection after game start and it will try to send saved FB events after connection appearance
 */

public static class AnalyticsManager
{
    // ============== PRIVATE FIELDS ==============
    private static Dictionary<string, Dictionary<string, object>> m_savedEvents;

    // ============== PUBLIC METHODS ============== 
    public static void Initialize()
    {
        m_savedEvents = new Dictionary<string, Dictionary<string, object>>();

        FB.Init(SendSavedEvents);
        GameAnalytics.Initialize();
    }

    // ====== ROUND START =======
    public static void FireRoundStartEvent(int level, int attempt)
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, level.ToString());

        var roundInfo = new Dictionary<string, object>();
        roundInfo["level"] = level;
        roundInfo["attempt"] = attempt;

        if (FB.IsInitialized)
        {
            FB.LogAppEvent("levelStart", parameters: roundInfo);
        }
        else
        {
            m_savedEvents.Add("levelStart", roundInfo);
        }
    }

    // ====== ROUND END =======
    public static void FireRoundEndEvent(int level, int attempt, int playSeconds)
    {
        GameAnalytics.NewDesignEvent("round_end");

        var roundInfo = new Dictionary<string, object>();
        roundInfo["level"] = level;
        roundInfo["attempt"] = attempt;
        roundInfo["playSeconds"] = playSeconds;

        if (FB.IsInitialized)
        {
            FB.LogAppEvent("levelEnd", parameters: roundInfo);
        }
        else
        {
            m_savedEvents.Add("levelEnd", roundInfo);
        }
    }

    // ====== ROUND COMPLETED =======
    public static void FireRoundCompleteEvent(int level, int attempt, int score)
    {
        GameAnalytics.NewProgressionEvent(
            GAProgressionStatus.Complete, level.ToString(), attempt.ToString(), score.ToString());

        var roundInfo = new Dictionary<string, object>();
        roundInfo["level"] = level;
        roundInfo["attempt"] = attempt;
        roundInfo["score"] = score;

        if (FB.IsInitialized)
        {
            FB.LogAppEvent("levelWin", parameters: roundInfo);
        }
        else
        {
            m_savedEvents.Add("levelWin", roundInfo);
        }
    }

    // ====== ROUND FAILED =======
    public static void FireRoundFailEvent(int level, int attempt, int score, float progress)
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail,
        level.ToString(), attempt.ToString(), progress.ToString(), score);

        var roundInfo = new Dictionary<string, object>();
        roundInfo["level"] = level;
        roundInfo["attempt"] = attempt;
        roundInfo["score"] = score;
        roundInfo["progress"] = progress;

        if (FB.IsInitialized)
        {
            FB.LogAppEvent("levelFail", parameters: roundInfo);
        }
        else
        {
            m_savedEvents.Add("levelFail", roundInfo);
        }
    }

    // ====== CUSTOM EVENT =======
    public static void FireSingleEvent(string singleEvent)
    {
        GameAnalytics.NewDesignEvent(singleEvent);

        if (FB.IsInitialized)
        {
            FB.LogAppEvent(singleEvent);
        }
        else
        {
            m_savedEvents.Add(singleEvent, null);
        }
    }

    // ============== PRIVATE METHODS ============== 
    private static void SendSavedEvents()
    {
        foreach (var e in m_savedEvents)
        {
            if (e.Value != null)
                FB.LogAppEvent(e.Key, parameters: e.Value);
            else
                FB.LogAppEvent(e.Key);
        }

        m_savedEvents.Clear();
    }
}
