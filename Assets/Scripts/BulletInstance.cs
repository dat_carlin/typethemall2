using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BulletInstance : MonoBehaviour
{
    [HideInInspector] public char m_letterChar;

    [SerializeField] private TextMeshPro m_letterTmp;
    [SerializeField] private GameObject m_letterSpark;
    private Rigidbody m_rigidbody;

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        m_letterChar = m_letterTmp.text[0];
    }

    public void SetupAndShootBullet(char letter)
    {
        SetupLetterTMP(letter);
        Fire();
    }

    private void SetupLetterTMP(char letter)
    {
        m_letterTmp.text = letter.ToString();
        m_letterChar = m_letterTmp.text[0];
    }

    void Fire()
    {
        m_rigidbody.velocity = transform.forward * 10f;
    }

    public void PlayBulletSpark()
    {
        Instantiate(m_letterSpark, transform.position, Quaternion.identity);
    }
}
